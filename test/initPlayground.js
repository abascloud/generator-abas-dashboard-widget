
const helpers = require('yeoman-test')
const assert = require('yeoman-assert')
const path = require('path')

describe('abas-dashboard-widget:initPlayground', function () {

    let generator = path.join(__dirname, '../generators/initPlayground');
    let testProject = "TestingSite";
    beforeEach(function () {
        return helpers.run(generator)
            .withOptions({
                skipInstall: true,
                playgroundProject: testProject
            })
    })

    it('generates a project folder', done => {
        process.chdir('../..');
        assert.file(testProject);
        assert.file(`${testProject}/dashboard`);
        done()
    });

});