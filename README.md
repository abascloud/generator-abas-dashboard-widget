# The abas dashboard widget CLI #

abas-dashboard-widget CLI is the official command line tool for abas dashboard widget projects.
It includes a boilerpate generator for creating widgets, a development dashboard and a server.
The CLI itself is a yeoman generator.


## Install aba-dashboard-widget CLI ##
1. Make sure you have installed the latest version of Node.js To check the version of Node.js that you have installed, run:

    ```bash
    node --version
    ```

1. Update npm.

    ```
    npm install npm@latest -g
    ```

1. Ensure that Git is installed. If it isn't, you can find it on the [Git downloads page](https://git-scm.com/downloads)

    ```
    git --version
    ```


1. Make git use only https for bitbucket

    ```
    git config --global url."https://bitbucket.org/".insteadOf git@bitbucket.org:
    ```

1. Install bower, polymer-cli, yo, bower-art-resolver and abas-dashboard-widget

    ```
    npm install -g bower polymer-cli yo bower-art-resolver generator-abas-dashboard-widget
    ```

    or

    ```
    yarn add global bower polymer-cli yo generator-abas-dashboard-widget
    ```

## Project Layout ##
This is an overview of the generated directories.

* Playground
    * dashboard
    * first-widget
    * another-widget
        * another-widget.html
        * widget.json
        * bower.json
        * assets

**Playground** serves as development environment for all of your widgets. The Playground contains a demo dashboard where you can add and test your widgets. The playground connects to your abas Installation. If your widget requests abas services the requests are automatically authorized and directed to your abas installation.

**dashboard** contains the demo dashboard application.

***-widget** Every widget you create has its own directory. These folders are what you manage with your SCM. It is mandatory to have each widgets in its own repository.
## Widget folder layout
* first-widget
    * first-widget.html
    * widget.json
    * bower.json
    * assets

**first-widget.html** Is the actually file of your web component

**widget.json** Contains the description of your widget.

**bower.json** The components dependencies.

**assets** This folder contains static resources of your component.

# Usage
Display the generator menu.
```bash
yo abas-dashboard-widget
```

# Getting Started
## Initialize the Playground
Either you start the generator with
```bash
yo abas-dashboard-widget
```
and select the option from the generator menu or you just enter
```bash
yo abas-dashboard-widget:initPlayground
```
## Creating a widget ##
```bash
yo abas-dashboard-widget:createWidget
```
## Starting up the playground ##
```bash
yo abas-dashboard-widget:playgroundUp
```
The demo dashboard is now running under http://[your-tenant].localhost.abas.ninja:8081/dashboard/ Log into the board with your usual credentials.
## Link an exisitng widget to the demo dashboard
If you work on an exisiting widget stored in a SCM repository you first need to clone the widget repository into the Playground project.
Then link the widget to the demo dashboard. To do so just start:
```bash
yo abas-dashboard-widget:linkWidget
```
Then choose the widget from the displayed list 
# Adding external libraries and components
Add the library or component to the widgets bower.json. To install the extension change directory to the widget folder and execute 'bower install'.
Since every widget comes with a .bowerrc by default the extension will be installed to the playground dashboard bower_components folder and you should
not need caring about dependency management.
# Working with an IDE
## Visual Studio Code ##
Microsoft Visual Code (https://code.visualstudio.com/) is free of charge and best suited to work on widgets. We recommend to install the additional plugins:
* Polymer IDE
* Polymer Syntax
* Polymer 2 Snippets
* Chai snippets
* ESLint
## Open the project in Visual Studio Code
From the menu choose File -> Open Folder and open the Playground directory.

# FAQ
If you have problems initializing your playground we recommend using the integrated terminal in visual studio code. 
If you are using git bash it could happen that you run into some problems that aren't solved yet.

# Support
If you experience problems with the abas dashboard widget generator contact us via the issue tracker in our bitbucket repository https://bitbucket.org/abascloud/generator-abas-dashboard-widget/issues