const Generator = require('yeoman-generator');
const util = require('util');
const _ = require('lodash');
const mkdirp = require('mkdirp');

module.exports = class WidgetGenerator extends Generator {

    constructor(args, opts) {
        super(args, opts);

        this.option('companyName', {
            type: String,
            desc: 'Developer Tag of the company for the widget name in snake case'
        });
        this.option('widgetName', {
            type: String,
            desc: 'Name of the widget in snake case'
        });
        this.option('withMiddlewareExample', {
            type: Boolean,
            desc: 'Add middleware example request'
        });
        this.option('developer_name', {
            type: String,
            desc: 'Developer name'
        });
        this.option('developer_email', {
            type: String,
            desc: 'Developer eMail'
        });
        this.option('developer_homepage', {
            type: String,
            desc: 'Developer homepage'
        });
    }

    _isValidName(name) {
        return typeof name === 'string' &&
            name.trim().length >= 3 &&
            name.match(/^[a-z]+([-]?[a-z]+)*$/);
    }

    _outputInvalidName(input) {
        if (input.trim().length < 3) {
            return 'Your name must be at least 3 characters long.';
        } else if (input !== input.toLowerCase()) {
            return 'All letters in the name must be lowercase.';
        } else if (/\d/.test(input)) {
            return 'The name must not contain any numbers.';
        } else if (input.startsWith('-')) {
            return 'The name must not start with a dash.';
        } else if (input.trim().length < input.length) {
            return 'The name must not contain spaces.'
        } else {
            return 'The name must not contain any special characters apart from dashes which should be always surrounded by letters.';
        }
    }

    initializing() {
        this.playgroundProject = this.config.get('playgroundProject');

        if (!this.playgroundProject) {
            this.composeWith(require.resolve('../initPlayground'));
        }
    }

    prompting() {
        return this.prompt([{
            name: 'companyName',
            type: 'input',
            store: true,
            message: 'What is the developer tag of your company?',
            validate: value => {
                if (!this._isValidName(value)) {
                    return 'Invalid company developer tag: "' + value + '". ' + this._outputInvalidName(value);
                }
                return true;
            }
        }, {
            name: 'widgetName',
            type: 'input',
            message: 'How do you want to name your widget?',
            transformer: (input, answers, options) => {
                return `${answers['companyName']}-${input}-widget`
            },
            validate: (value, answers) => {
                var temp = value.replace(`${answers['companyName']}-`, '');
                var tempValue = temp.replace('-widget', '');

                if (!this._isValidName(tempValue)) {
                    return 'Invalid name: "' + tempValue + '". ' + this._outputInvalidName(tempValue);
                }
                return true;
            },
            filter: (input, answers) => { return `${answers['companyName']}-${input}-widget` }
        }, {
            name: 'withMiddlewareExample',
            type: 'confirm',
            default: true,
            message: 'Add middleware example request',
        }, {
            name: 'developer_name',
            type: 'input',
            store: true,
            message: 'Who are you?'
        }, {
            name: 'developer_email',
            type: 'input',
            store: true,
            message: 'Tell me your eMail',
            validate: function(value) {
                let eMailFormat = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
                return eMailFormat.test(value) ? true : 'Given string is not an eMail address';
            }
        }, {
            name: 'developer_homepage',
            type: 'input',
            store: true,
            message: 'Your homepage'
        }]).then(answers => {
            this.options = Object.assign({}, this.options, answers);
        });
    }

    configuring() {
        this.widgetFolder = this.destinationPath(this.playgroundProject, this.options.widgetName);
        this.options.installDependencies = true;
        this.options.previewImage = `preview-${(Math.floor(Math.random() * 10) % 4)}.png`;
    }

    writing() {

        this.options['widget_class'] = _.upperFirst(_.camelCase(this.options.widgetName));

        // setup bower.json
        this.fs.copyTpl(this.templatePath('bower.ejs'), `${this.widgetFolder}/bower.json`, this.options);

        // widget.html
        this.fs.copyTpl(
            this.templatePath('widget.ejs'),
            util.format('%s/%s.html', this.widgetFolder, this.options.widgetName),
            this.options);

        // widget.html
        this.fs.copyTpl(
            this.templatePath('widget-settings.ejs'),
            `${this.widgetFolder}/${this.options.widgetName}-settings.html`,
            this.options);

        // widget.json
        this.fs.copyTpl(
            this.templatePath('library.ejs'),
            util.format('%s/widget.json', this.widgetFolder),
            this.options)

        // copy widget assets
        this.fs.copy(this.templatePath(`assets/${this.options.previewImage}`), `${this.widgetFolder}/assets/preview.png`);

        //copy locales.json
        this.fs.copy(this.templatePath('locales.json'), `${this.widgetFolder}/locales.json`);

        //Copy gitignore
        this.fs.copy(this.templatePath('gitignore.ejs'), `${this.widgetFolder}/.gitignore`);

        //Copy Pipeline Files
        this.fs.copy(this.templatePath('bitbucket-pipelines.yml'), `${this.widgetFolder}/bitbucket-pipelines.yml`);
        this.fs.copy(this.templatePath('wct.conf.chrome.json'), `${this.widgetFolder}/wct.conf.chrome.json`);
        this.fs.copy(this.templatePath('wct.conf.chrome.local.json'), `${this.widgetFolder}/wct.conf.chrome.local.json`);

        //make folder for tests and add empty file tp keep the folder
        mkdirp(`${this.widgetFolder}/test-results/`);
        this.fs.copy(this.templatePath('gitignore.ejs'), `${this.widgetFolder}/test-results/.gitignore`);
    }

    install() {
        // do nothing here
        // bower install is started by linkWidget
    }

    end() {
        this.composeWith('abas-dashboard-widget:linkWidget', { widgetToLink: this.options.widgetName, skipInstall: true });
    }
};