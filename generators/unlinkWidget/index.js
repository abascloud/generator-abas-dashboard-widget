const Generator = require('yeoman-generator');
const _fs = require('fs');
const replace = require('replace-in-file');

module.exports = class extends Generator {
    constructor(args, opts) {
        super(args, opts);
        this.option('widgetToUnlink', {
            type: String,
            desc: 'Name of the widget to unlink'
        });
        this.linkJobs = []
    }
    initializing() {
        this.playgroundProject = this.config.get('playgroundProject');
        if (!this.playgroundProject) {
            this.log('Initialize playground project first. Bye!');
            process.exit();
        }
        this.widgetDirectories = _fs.readdirSync(this.destinationPath(this.playgroundProject)).filter((entry) => {
            return (_fs.lstatSync(this.destinationPath(this.playgroundProject, entry)).isDirectory() &&
                _fs.existsSync(this.destinationPath(this.playgroundProject, entry, 'widget.json')));
        });
        if (this.widgetDirectories.length === 0) {
            this.log('No widget found to unlink from the dashboard playground');
            process.exit();
        }

        if (this.options.widgetToUnlink) {
            this.skipPrompting = true;
        }
    }

    prompting() {
        if (this.skipPrompting) {
            return;
        }
        return this.prompt([{
            name: 'widgetToUnlink',
            type: 'list',
            store: false,
            message: 'Choose a widget to unlink from the playground dashboard',
            choices: this.widgetDirectories.map((dir) => {
                return {
                    name: dir,
                    value: dir
                };
            }).concat({
                name: 'Unlink them all',
                value: 'all'
            })
        }]).then(answers => {
            this.options = answers;
        });
    }

    configuring() {
        if (this.options.widgetToUnlink === 'all') {
            this.widgetDirectories.forEach((dir) => {
                this.linkJobs.push(this._createUnlinkJob(dir))
            })
        } else {
            this.linkJobs.push(this._createUnlinkJob(this.options.widgetToUnlink))
        }
    }

    writing() {
        this.linkJobs.forEach((job) => { this._unlinkWidget(job) });
    }

    _createUnlinkJob(widgetToUnlink) {
        return {
            widgetToUnlink: widgetToUnlink,
            widgetJsonLinkPath: `bower_components/${widgetToUnlink}/widget.json`,
            widgetToUnlinkDir: this.destinationPath(this.playgroundProject, widgetToUnlink),
            dashboardDir: this.destinationPath(this.playgroundProject, 'dashboard')
        }
    }

    _unlinkWidget(linkJob) {
        this.log(`Unlinking ${linkJob.widgetToUnlink} from playground dashboard`);
        
        let workingDir = process.cwd();
        
        process.chdir(linkJob.dashboardDir);
        this.spawnCommandSync('bower', ['uninstall', linkJob.widgetToUnlink])

        process.chdir(workingDir);

        // Update abas-dashboard-webui-app.html
        // remove widget from library
        const appHtmlPath = `${linkJob.dashboardDir}/abas-dashboard-webui-app.html`;
        const hook = new RegExp(`<aba-widgetlibrary url="${linkJob.widgetJsonLinkPath}"></aba-widgetlibrary>`, 'g');
        const replacement = '';
        const options = {
            files: appHtmlPath,
            from: hook,
            to: replacement
        };
        try {
            replace.sync(options)
        } catch (error) {
            this.log(`An error occured while adding the widget library to ${appHtmlPath}\n${error}`)
        }
    }
};