const Generator = require('yeoman-generator');
const _fs = require('fs');
const replace = require('replace-in-file');


module.exports = class extends Generator {
    constructor(args, opts) {
        super(args, opts);
        this.option('widgetToLink', {
            type: String,
            desc: 'Name of the widget to link'
        });
        this.linkJobs = []
    }
    initializing() {
        this.playgroundProject = this.config.get('playgroundProject');
        if (!this.playgroundProject) {
            this.log('Initialize playground project first. Bye!');
            process.exit();
        }
        this.widgetDirectories = _fs.readdirSync(this.destinationPath(this.playgroundProject)).filter((entry) => {
            return (_fs.lstatSync(this.destinationPath(this.playgroundProject, entry)).isDirectory() &&
                _fs.existsSync(this.destinationPath(this.playgroundProject, entry, 'widget.json')));
        });
        if (this.widgetDirectories.length === 0) {
            this.log('No widget found to link with the dashboard playground');
            process.exit();
        }

        if (this.options.widgetToLink) {
            this.skipPrompting = true;
        }
    }

    prompting() {
        if (this.skipPrompting) {
            return;
        }
        return this.prompt([{
            name: 'widgetToLink',
            type: 'list',
            store: false,
            message: 'Choose a widget to link with the playground dashboard',
            choices: this.widgetDirectories.map((dir) => {
                return {
                    name: dir,
                    value: dir
                };
            }).concat({
                name: 'Link them all',
                value: 'all'
            })
        }]).then(answers => {
            this.options = answers;
        });
    }

    configuring() {
        if (this.options.widgetToLink === 'all') {
            this.widgetDirectories.forEach((dir) => {
                this.linkJobs.push(this._createLinkJob(dir))
            })
        } else {
            this.linkJobs.push(this._createLinkJob(this.options.widgetToLink))
        }
        this.dashboardPath = `${this.destinationPath()}/${this.playgroundProject}/dashboard`;
    }

    writing() {    
        this.linkJobs.forEach((job) => { this._linkWidget(job) });
    }

    _createLinkJob(widgetToLink) {
        return {
            widgetToLink: widgetToLink,
            widgetJsonLinkPath: `bower_components/${widgetToLink}/widget.json`,
            widgetToLinkDir: this.destinationPath(this.playgroundProject, widgetToLink),
            dashboardDir: this.destinationPath(this.playgroundProject, 'dashboard')
        }
    }

    _linkWidget(linkJob) {

        // Create widget/.bowerrc suitable for this playgroud
        this.fs.copyTpl(this.templatePath('bowerrc.ejs'), `${linkJob.widgetToLinkDir}/.bowerrc`);

        //Copy gitignore
        this.fs.copy(this.templatePath('gitignore.ejs'), `${linkJob.widgetToLinkDir}/.gitignore`);

        this.log(`Linking ${linkJob.widgetToLink} with playground dashboard`);

        let workingDir = process.cwd();

        process.chdir(linkJob.widgetToLinkDir);
        this.spawnCommandSync('bower', ['link']);

        process.chdir(linkJob.dashboardDir);
        this.spawnCommandSync('bower', ['link', linkJob.widgetToLink])

        process.chdir(workingDir);

        // Update abas-dashboard-webui-app.html
        // Add widget library
        const appHtmlPath = `${linkJob.dashboardDir}/abas-dashboard-webui-app.html`;
        const hook = '<!-- playground-widgetlibrary-extension-point -->';
        const replacement = `${hook}\n\t\t<aba-widgetlibrary url="${linkJob.widgetJsonLinkPath}"></aba-widgetlibrary>`;
        const options = {
            files: appHtmlPath,
            from: hook,
            to: replacement
        };
        try {
            replace.sync(options)
        } catch (error) {
            this.log(`An error occured while adding the widget library to ${appHtmlPath}\n${error}`)
        }
    }
};