const Generator = require('yeoman-generator');
const util = require('util');
const _ = require('lodash');
const _fs = require('fs');

module.exports = class extends Generator {

	constructor(args, opts) {
		super(args, opts);

		this.option('generator', {
			type: 'String',
			desc: 'Choose generator'
		});
	}

	prompting() {
		return this.prompt([{
			name: 'generator',
			type: 'list',
			store: false,
			message: 'Hello! This is the abas dashboard CLI. What can i do for you?',
			choices: [
				{
					name: 'Initialize playground',
					value: 'abas-dashboard-widget:initPlayground'
				}, {
					name: 'Create widget and link it to the playground dashboard',
					value: 'abas-dashboard-widget:createWidget'
				}, {
					name: 'Link widget',
					value: 'abas-dashboard-widget:linkWidget'
				}, {
					name: 'Unlink widget',
					value: 'abas-dashboard-widget:unlinkWidget'
				}, {
					name: 'Start playground server',
					value: 'abas-dashboard-widget:playgroundUp'
				}
			]
		}]).then(answers => {
			this.options = answers;
		});
	}

	writing() {
		this.composeWith(this.options.generator);
	}

}
