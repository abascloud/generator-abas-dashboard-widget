const Generator = require('yeoman-generator');
const _fs = require('fs');
const fsExtra = require('fs-extra');
const https = require('https');

module.exports = class extends Generator {

	constructor(args, opts) {
		super(args, opts);
		this.option('playgroundProject', {
			type: String,
			desc: 'The superordinate playground project gives you the option to develop one or more widgets.'
		});
		this.option('forceInit', {
			type: Boolean,
			desc: 'Reinitialize over an existing playground',
			default: false
		});
		this.option('wantCloudTenant', {
			type: Boolean,
			desc: 'Use your own cloud tenant',
			default: false
		});
		this.option('cloudTenant', {
			type: String,
			desc: 'Cloud tenant',
			default: 'crystal-labs'
		});
		this.option('domain', {
			type: String,
			desc: 'Domain',
			default: 'eu.abas.ninja'
		});
	}

	_makePlaygroundPath(projectName) {
		return `${this.destinationPath()}/${projectName}`;
	}

	_isValidProjectName(name) {
		return typeof name === 'string'
			&& name.trim().length > 2
			&& name.match(/^[a-z]+([-]?[a-z]+)*$/i);
	}

	_isValidTenantNameForm(name) {
		return typeof name === 'string'
			&& name.match(/[a-z][a-z0-9-]{2,54}/igm);
	}

	_isValidURLForm(name) {
		return typeof name === 'string'
			&& name.match(/^(?:[^@\/\n]+@)?(?:www\.)?([^:\/\n]+)/igm);
	}

	// *Does not work on windows must be done manually* this is necessary, because if the abas dependencies (in bower) are declared using git@bitbucket, it will SSH to it, and will require authentication. HTTPS supports anonymous.
	/*_gitReplaceForBowerDependencies() {
		this.spawnCommandSync('git', ['config', '--global', 'url."https://bitbucket.org/".insteadOf', 'git@bitbucket.org:']);
	}*/

	_writeConfigurationFile() {
		var file = _fs.createWriteStream(`${this.playgroundProjectPath}/dashboard/configuration.json`);
		var request = https.get(`https://${this.options.cloudTenant}.${this.options.domain}/configuration.json`, function (response) {

			if (response.statusCode != 200) {
				// we should never see this since we validated it before
				this.log(`The URL https://${this.options.cloudTenant}.${this.options.domain}/configuration.json did not work.`);
			} else {
				response.pipe(file);
			}
		});
	}

	_writeRunFile(extension, bash) {
		this.fs.copyTpl(
			this.templatePath('run/run.ejs'),
			this.destinationPath(`${this.playgroundProjectPath}/dashboard/run.${extension}`), {
				bash: (bash ?bash :""),
				cloudTenant: this.options.cloudTenant,
				domain: this.options.domain
			});
	}

	prompting() {
		return this.prompt([{
			name: 'playgroundProject',
			type: 'input',
			store: true,
			message: 'Enter the project name. The superordinate project gives you the option to develop several widgets within a project.',
			default: 'myWidgetsProject',
			when: _asnwers => {
				return !this._isValidProjectName(this.options.playgroundProject)
			},
			validate: value => {
				if (!this._isValidProjectName(value)) {
					return 'Project name must be a non empty string containing alphanumeric characters and/or dashes in between. Minimum 3 characters';
				}
				return true;
			}
		}, {
			name: 'forceInit',
			type: 'confirm',
			store: false,
			message: (answers) => {
				return `The playground ${answers.playgroundProject} already exists. Make a fresh start?`
			},
			default: true,
			when: (answers) => {
				return (fsExtra.pathExistsSync(this._makePlaygroundPath(answers.playgroundProject)))
			}
		}, {
			name: 'wantCloudTenant',
			type: 'list',
			store: false,
			message: `Do you want to use your own cloud tenant or the default one [https://${this.options.cloudTenant}.${this.options.domain}] ? (uptime and access not guaranteed)`,
			choices: [
				{ name: 'I want to use my own tenant (input required at next step)', value: true },
				{ name: 'I do not have a cloud tenant, I will use the default one.', value: false }],
			default: 0
		}, {
			name: 'cloudTenant',
			type: 'input',
			store: true,
			message: 'Enter your cloud tenant name, used for the configuration.json, for getting test data (https://{CLOUD_TENANT}.{domain})',
			default: this.options.cloudTenant,
			validate: value => {
				return this._isValidTenantNameForm(value) ? true : 'Not a valid tenant name (letters + numbers + dash, no dot, no space)';
			},
			when: (answers) => {
				return answers.wantCloudTenant
			}
		}, {
			name: 'domain',
			type: 'input',
			store: true,
			message: (answers) => {
				return `Enter the domain of your cloud tenant, used for the configuration.json, for getting test data (https://${answers.cloudTenant}.{DOMAIN})`
			},
			default: this.options.domain,
			validate: (value, answers) => {
				if (!this._isValidURLForm(value)) {
					return 'Not a valid domain name';
				}
				return new Promise((res, rej) => {
					let errorMessage = `\nThe URL https://${answers.cloudTenant}.${value}/configuration.json did not work.`;
					var request = https.get(`https://${answers.cloudTenant}.${value}/configuration.json`, function (response) {
						if (response.statusCode != 200) {
							return rej(errorMessage);
						} else {
							res(true);
						}
					});
					request.on('error', (e) => {
						rej(errorMessage);
					});
				});
			},
			when: (answers) => {
				return answers.wantCloudTenant
			}
		}]).then(answers => {
			this.options = Object.assign({}, this.options, answers);
		});
	}

	configuring() {
		this.playgroundProjectPath = `${this.destinationPath()}/${this.options.playgroundProject}`;
		this.dashboardPath = `${this.playgroundProjectPath}/dashboard`;
		this.config.set('playgroundProject', this.options.playgroundProject);
		this.config.save();
	}

	writing() {
		if (_fs.existsSync(this.playgroundProjectPath) && !this.options.forceInit) {
			this.log(`Super project ${this.options.playgroundProject} seem to exist. Skipping super project initialization.`);
			return
		}
		// prepare the dashboard
		fsExtra.removeSync(`${this.playgroundProjectPath}/dashboard`);
		fsExtra.ensureDirSync(`${this.playgroundProjectPath}/dashboard`);

        this.fs.copy(`${this.templatePath('dashboard')}/**`, `${this.playgroundProjectPath}/dashboard`);
        this.fs.copy(`${this.templatePath('dashboard')}/.bowerrc`, `${this.playgroundProjectPath}/dashboard/.bowerrc`);

		this._writeRunFile("sh", "#! /bin/bash");
		this._writeRunFile("cmd");
		this._writeConfigurationFile();
		//this seems not to work on windows 10 user has to do it manually
		//this._gitReplaceForBowerDependencies();
		this.fs.copy(this.templatePath('gitconfig/gitignore'), `${this.playgroundProjectPath}/dashboard/.gitignore`);
	}

	install() {
		process.chdir(this.dashboardPath);
		this.installDependencies({
			bower: true,
			npm: false,
			yarn: false
		})
	}

	end() {
		this.log('Development environment is ready! Now create some widgets.');
	}

};
