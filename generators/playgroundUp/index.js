const Generator = require('yeoman-generator');
module.exports = class extends Generator {
    constructor(args, opts) {
        super(args, opts);
    }
    initializing() {
        this.playgroundProject = this.config.get('playgroundProject');
        if (!this.playgroundProject) {
            this.log('Initialize playground project first. Bye!');
            process.exit();
        }
    }

    up() {
        process.chdir(this.destinationPath(this.playgroundProject, 'dashboard'));
        if (/^win/.test(process.platform)) {
            this.spawnCommand('./run.cmd');
        } else {
            this.spawnCommand('./run.sh');
        }
    }
}